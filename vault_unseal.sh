#!/bin/bash

kubectl -n vault exec vault-0 -- vault operator init -key-shares=1 -key-threshold=1 -format=json > cluster_keys.json || true

export VAULT_UNSEAL_KEY=$(jq -r ".unseal_keys_b64[]" cluster_keys.json)

VAULT_UNSEAL_KEY=$(jq -r ".unseal_keys_b64[]" cluster_keys.json)